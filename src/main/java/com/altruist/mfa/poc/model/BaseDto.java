package com.altruist.mfa.poc.model;

import java.util.Date;
import java.util.UUID;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;

@Data
@JsonInclude(value = Include.NON_NULL)
public class BaseDto {

  private UUID id;
  private Date dateTimeCreated;
  private Date dateTimUpdated;


}
