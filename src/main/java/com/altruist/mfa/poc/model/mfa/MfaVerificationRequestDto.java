package com.altruist.mfa.poc.model.mfa;

import java.util.UUID;
import com.altruist.mfa.poc.mfaclient.MfaClient.MfaType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@JsonInclude(value = Include.NON_NULL)
public class MfaVerificationRequestDto extends BaseMfaDto {

  // Used to determine the MFA client if mfaClientName in base class isnt set
  private MfaType mfaType;
  private String destination;
  // If not passed when starting a verification, the MFA client should initialize this to an
  // injected default value
  private String verificationServiceId;
  private String verificationRequestId;
  private Boolean validated;
  private UUID userAccountId;
  private UUID overrideId;
}
