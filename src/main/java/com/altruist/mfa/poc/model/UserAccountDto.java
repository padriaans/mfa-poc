package com.altruist.mfa.poc.model;

import com.altruist.mfa.poc.mfaclient.MfaClient.MfaType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@JsonInclude(value = Include.NON_NULL)
public class UserAccountDto extends BaseDto {

  private String firstName;
  private String lastName;
  private String username;
  private String password;
  private String emailAddress;
  private String phoneNumber;
  private Boolean useMfa = false;
  private MfaType mfaType;

}
