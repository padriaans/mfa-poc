package com.altruist.mfa.poc.model.mfa;

import java.util.UUID;
import com.altruist.mfa.poc.mfaclient.MfaClient.MfaType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@JsonInclude(value = Include.NON_NULL)
public class MfaVerificationValidationDto extends BaseMfaDto {

  // Input when validating
  private String verificationServiceId;
  private String token;
  private String destination;
  // Will be populated for output
  private MfaType mfaType;
  private String verificationValidationId;
  private String verificationRequestId;
  private Boolean validated;
  private UUID overrideId;
  // FKs
  private UUID userAccountId;
  private UUID mfaVerificationRequestId;
}
