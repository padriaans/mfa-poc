package com.altruist.mfa.poc.model.mfa;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@JsonInclude(value = Include.NON_NULL)
public class MfaVerificationServiceDto extends BaseMfaDto {

  private String serviceId;
  // 30 chars max for Twilio Verify
  private String serviceName;
  private Integer codeLength;
}
