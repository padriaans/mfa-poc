package com.altruist.mfa.poc.model.mfa;

import com.altruist.mfa.poc.mfaclient.MfaClient;
import com.altruist.mfa.poc.model.BaseDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@JsonInclude(value = Include.NON_NULL)
public class BaseMfaDto extends BaseDto {

  // If set, will drive the MFA client selection directly, if not, mfaType in the extending classes
  // will
  private MfaClient.MfaClientName mfaClientName;

}
