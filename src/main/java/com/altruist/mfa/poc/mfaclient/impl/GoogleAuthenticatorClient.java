package com.altruist.mfa.poc.mfaclient.impl;

import com.altruist.mfa.poc.mfaclient.MfaClient;
import com.altruist.mfa.poc.model.mfa.MfaVerificationRequestDto;
import com.altruist.mfa.poc.model.mfa.MfaVerificationServiceDto;
import com.altruist.mfa.poc.model.mfa.MfaVerificationValidationDto;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import de.taimos.totp.TOTP;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base32;
import org.apache.commons.codec.binary.Hex;
import org.aspectj.apache.bcel.classfile.Code;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class GoogleAuthenticatorClient implements MfaClient {

  @Value("${mfa.poc.google.autenticator.secret.key}")
  private String secretKey;
  @Value("${mfa.poc.google.autenticator.thread.sleep.time:1000}")
  private Integer threadSleepTimeMills;
  @Value("${mfa.poc.google.autenticator.bar.code.issuer}")
  private String barCodeIssuer;
  @Value("${mfa.poc.google.autenticator.qr.code.height.pixels:250}")
  private Integer qrCodeHeightPixels;
  @Value("${mfa.poc.google.autenticator.qr.code.width.pixels:250}")
  private Integer qrCodeWidthPixels;

  private String currentCode = null;
  private Object currentCodeMutex = new Object();
  private Boolean isRunning = true;
  private Thread thread = null;

  @Override
  public MfaVerificationServiceDto createVerificationService(MfaVerificationServiceDto verificationService) throws Exception {
    return null;
  }

  @Override
  public MfaVerificationRequestDto sendVerificationToken(MfaVerificationRequestDto mfaVerificationRequestDto) throws Exception {
    return null;
  }

  @Override
  public MfaVerificationRequestDto fetchVerification(MfaVerificationRequestDto mfaVerificationRequestDto) throws Exception {
    return null;
  }

  @Override
  public MfaVerificationRequestDto approveVerification(MfaVerificationRequestDto mfaVerificationRequestDto) throws Exception {
    return null;
  }

  @Override
  public MfaVerificationRequestDto cancelVerification(MfaVerificationRequestDto mfaVerificationRequestDto) throws Exception {
    return null;
  }

  @Override
  public MfaVerificationValidationDto validateVerificationToken(MfaVerificationValidationDto mfaVerificationValidationDto)
      throws Exception {
    String token = mfaVerificationValidationDto.getToken();
    if(token.equals(this.getCurrentCode())) {
      mfaVerificationValidationDto.setValidated(true);
    } else {
      mfaVerificationValidationDto.setValidated(false);
    }
    // Generate an ID, as it's reqrired to persist the validation record
    mfaVerificationValidationDto.setVerificationValidationId(UUID.randomUUID().toString());
    return mfaVerificationValidationDto;

  }

  protected static String generateSecretKey() {
    SecureRandom random = new SecureRandom();
    byte[] bytes = new byte[20];
    random.nextBytes(bytes);
    Base32 base32 = new Base32();
    return base32.encodeToString(bytes);
  }

  protected String getTOTPCode() {
    Base32 base32 = new Base32();
    byte[] bytes = base32.decode(secretKey);
    String hexKey = Hex.encodeHexString(bytes);
    return TOTP.getOTP(hexKey);
  }

  @PostConstruct
  public void init() {
    log.info("Google authenticator: initializing");
    currentCode = getTOTPCode();
    log.info("Google authenticator: initial code: " + currentCode);
    thread = new Thread(() -> {
      while (isRunning) {
        updateCurrentCode(getTOTPCode());
        try {
          Thread.sleep(threadSleepTimeMills);
        } catch (InterruptedException e) {
        }
        ;
      }
    });
    thread.start();
    log.info("Google authenticator: running");
  }

  @PreDestroy
  public void destroy() {
    log.info("Google authenticator: stopping thread");
    isRunning = false;
    try {
      thread.join();
    } catch (InterruptedException e) {
    }
    ;
    log.info("Google authenticator: joined with thread");

  }

  protected String getCurrentCode() {
    synchronized (currentCodeMutex) {
      return currentCode;
    }
  }

  protected void updateCurrentCode(String code) {
    synchronized (currentCodeMutex) {
      if (!currentCode.equals(code)) {
        currentCode = code;
        log.info("Google authenticator: code now " + currentCode);
      }
    }
  }

  public String getGoogleAuthenticatorBarCode(String account) {
    try {
      return "otpauth://totp/"
          + URLEncoder.encode(barCodeIssuer + ":" + account, "UTF-8").replace("+", "%20")
          + "?secret=" + URLEncoder.encode(secretKey, "UTF-8").replace("+", "%20")
          + "&issuer=" + URLEncoder.encode(barCodeIssuer, "UTF-8").replace("+", "%20");
    } catch (UnsupportedEncodingException e) {
      throw new IllegalStateException(e);
    }
  }

  public byte[] createQRCode(String account)
      throws Exception {
    String barCodeData = getGoogleAuthenticatorBarCode(account);
    BitMatrix matrix = new MultiFormatWriter().encode(barCodeData, BarcodeFormat.QR_CODE,
        qrCodeWidthPixels, qrCodeHeightPixels);
    try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
      MatrixToImageWriter.writeToStream(matrix, "png", os);
      return os.toByteArray();
    }
  }

  public static void main(String[] args) {
    System.out.println(generateSecretKey());
  }
}
