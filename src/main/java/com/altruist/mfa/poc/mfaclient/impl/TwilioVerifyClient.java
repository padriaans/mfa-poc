package com.altruist.mfa.poc.mfaclient.impl;

import com.altruist.mfa.poc.mfaclient.MfaClient;
import com.altruist.mfa.poc.model.mfa.MfaVerificationRequestDto;
import com.altruist.mfa.poc.model.mfa.MfaVerificationServiceDto;
import com.altruist.mfa.poc.model.mfa.MfaVerificationValidationDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twilio.Twilio;
import com.twilio.rest.verify.v2.Service;
import com.twilio.rest.verify.v2.ServiceCreator;
import com.twilio.rest.verify.v2.service.Verification;
import com.twilio.rest.verify.v2.service.Verification.Status;
import com.twilio.rest.verify.v2.service.VerificationCheck;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class TwilioVerifyClient implements MfaClient {

  // How a positive token validation is communicated back for this client
  private static final String APPROVED = "approved";

  @Autowired
  private ObjectMapper objectMapper;

  public enum VerifyChannel {
    SMS("sms"), CALL("call"), EMAIL("email");

    private final String value;

    private VerifyChannel(String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }
  }

  @Value("${mfa.poc.trilio.account.sid}")
  private String accountSid;
  @Value("${mfa.poc.trilio.auth.token}")
  private String authToken;
  @Value("${mfa.poc.trilio.default.verification.service.id}")
  private String defaultVerificationServiceId;

  @PostConstruct
  public void init() {
    log.info("Initializing Twilio");
    Twilio.init(accountSid, authToken);
    log.info("Twilio initialized");
  }

  @Override
  public MfaVerificationServiceDto createVerificationService(MfaVerificationServiceDto verificationService) throws Exception {
    log.info("Creating a verification service with name " + verificationService.getServiceName() + " and code length " + verificationService.getCodeLength());
    ServiceCreator serviceCreator = Service.creator(verificationService.getServiceName()).setCodeLength(verificationService.getCodeLength());
    Service service = serviceCreator.create();
    log.info("Service created: " + objectMapper.writeValueAsString(service));
    verificationService.setServiceId(service.getSid());
    return verificationService;
  }

  protected VerifyChannel getVerifyChannel(MfaType mfaType) {
    if(mfaType.equals(MfaType.SMS)) {
      return VerifyChannel.SMS;
    } else if(mfaType.equals(MfaType.EMAIL)) {
      return VerifyChannel.EMAIL;
    } else if(mfaType.equals(MfaType.VOICE)) {
      return VerifyChannel.CALL;
    }
    return null;
  }

  @Override
  public MfaVerificationRequestDto sendVerificationToken(MfaVerificationRequestDto MfaVerificationRequestDto) throws Exception {
    String destination = MfaVerificationRequestDto.getDestination();
    String serviceId = MfaVerificationRequestDto.getVerificationServiceId();
    if( StringUtils.isEmpty(serviceId)) {
      serviceId = this.defaultVerificationServiceId;
      MfaVerificationRequestDto.setVerificationServiceId(serviceId);
    }
    VerifyChannel verifyChannel = this.getVerifyChannel(MfaVerificationRequestDto.getMfaType());
    log.info("Initiating MFA verification of type " + MfaVerificationRequestDto.getMfaType() + " to destination " + destination + " through service " + serviceId);
    Verification verification = Verification.creator(serviceId, destination, verifyChannel.getValue()).create();
    log.info("Verification initiated: " + objectMapper.writeValueAsString(verification));
    MfaVerificationRequestDto.setVerificationRequestId(verification.getSid());
    MfaVerificationRequestDto.setValidated(false);
    return MfaVerificationRequestDto;
  }

  @Override
  public MfaVerificationValidationDto validateVerificationToken(MfaVerificationValidationDto mfaVerificationValidationDto) throws Exception {
    String token = mfaVerificationValidationDto.getToken();
    String destination = mfaVerificationValidationDto.getDestination();
    String serviceId = mfaVerificationValidationDto.getVerificationServiceId();
    log.info("Validating MFA token " +  token + " sent to destination " +  destination + " through service " +  serviceId);
    VerificationCheck verificationCheck = VerificationCheck.creator(serviceId,token).setTo(destination).create();
    log.info("Verification results: " + objectMapper.writeValueAsString(verificationCheck));
    mfaVerificationValidationDto.setVerificationValidationId(verificationCheck.getSid());
    if(verificationCheck.getStatus().equals(APPROVED)) {
      mfaVerificationValidationDto.setValidated(true);
    } else {
      mfaVerificationValidationDto.setValidated(false);
    }
    return mfaVerificationValidationDto;
  }

  @Override
  public MfaVerificationRequestDto fetchVerification(MfaVerificationRequestDto mfaVerificationRequestDto) throws Exception {
    String serviceId = mfaVerificationRequestDto.getVerificationServiceId();
    String verificationRequestId = mfaVerificationRequestDto.getVerificationRequestId();
    log.info("Fetching MFA verification request " + verificationRequestId + " for service " +  serviceId);
    Verification verification = Verification.fetcher(serviceId,verificationRequestId).fetch();
    if(verification != null) {
      log.info("Verification retrieved: " + objectMapper.writeValueAsString(verification));
      if(verification.getStatus().equals(APPROVED)) {
        mfaVerificationRequestDto.setValidated(true);
      } else {
        mfaVerificationRequestDto.setValidated(false);
      }
      return mfaVerificationRequestDto;
    } else {
      log.info("Could not retrieve verification");
    }
    return null;
  }

  @Override
  public MfaVerificationRequestDto approveVerification(MfaVerificationRequestDto mfaVerificationRequestDto) throws Exception {
    String serviceId = mfaVerificationRequestDto.getVerificationServiceId();
    String verificationRequestId = mfaVerificationRequestDto.getVerificationRequestId();
    log.info("Approving verification request " + verificationRequestId + " for service " + serviceId);
    Verification verification = Verification.updater(serviceId,verificationRequestId,Verification.Status.APPROVED).update();
    if(verification != null) {
      log.info("Verification updated: " + objectMapper.writeValueAsString(verification));
      if(verification.getStatus().equals(APPROVED)) {
        mfaVerificationRequestDto.setValidated(true);
      } else {
        mfaVerificationRequestDto.setValidated(false);
      }
      return mfaVerificationRequestDto;
    } else {
      log.info("Could not update verification");
    }
    return null;
  }

  @Override
  public MfaVerificationRequestDto cancelVerification(MfaVerificationRequestDto mfaVerificationRequestDto) throws Exception {
    String serviceId = mfaVerificationRequestDto.getVerificationServiceId();
    String verificationRequestId = mfaVerificationRequestDto.getVerificationRequestId();
    log.info("Canceling verification request " + verificationRequestId + " for service " + serviceId);
    Verification verification = Verification.updater(serviceId,verificationRequestId, Status.CANCELED).update();
    if(verification != null) {
      log.info("Verification updated: " + objectMapper.writeValueAsString(verification));
      if(verification.getStatus().equals(APPROVED)) {
        mfaVerificationRequestDto.setValidated(true);
      } else {
        mfaVerificationRequestDto.setValidated(false);
      }
      return mfaVerificationRequestDto;
    } else {
      log.info("Could not update verification");
    }
    return null;
  }


}
