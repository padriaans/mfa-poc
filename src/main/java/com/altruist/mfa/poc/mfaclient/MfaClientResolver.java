package com.altruist.mfa.poc.mfaclient;

import com.altruist.mfa.poc.exception.BusinessException;
import com.altruist.mfa.poc.mfaclient.MfaClient.MfaClientName;
import com.altruist.mfa.poc.mfaclient.MfaClient.MfaType;
import com.altruist.mfa.poc.mfaclient.impl.GoogleAuthenticatorClient;
import com.altruist.mfa.poc.mfaclient.impl.TwilioVerifyClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MfaClientResolver {

  private TwilioVerifyClient twilioVerifyClient;
  private GoogleAuthenticatorClient googleAuthenticatorClient;

  @Autowired
  public MfaClientResolver(TwilioVerifyClient twilioVerifyClient, GoogleAuthenticatorClient googleAuthenticatorClient) {
    this.twilioVerifyClient = twilioVerifyClient;
    this.googleAuthenticatorClient = googleAuthenticatorClient;
  }

  public MfaClient getMfaClient(MfaClientName mfaClientName) throws BusinessException {
    if(mfaClientName.equals(MfaClientName.TWILIO_VERIFY)) {
      return twilioVerifyClient;
    }
    if(mfaClientName.equals(MfaClientName.GOOGLE_AUTHENTICATOR)) {
      return googleAuthenticatorClient;
    }
    throw new BusinessException("No MfaClient associated with " + mfaClientName);
  }

  public MfaClient getMfaClient(MfaType mfaType) throws BusinessException {
    return getMfaClient(getMfaClientName(mfaType));
  }

  public MfaClientName getMfaClientName(MfaType mfaType) throws BusinessException {
    if(mfaType.equals(MfaType.SMS)) {
      return MfaClientName.TWILIO_VERIFY;
    }
    if(mfaType.equals(MfaType.EMAIL)) {
      return MfaClientName.TWILIO_VERIFY;
    }
    if(mfaType.equals(MfaType.VOICE)) {
      return MfaClientName.TWILIO_VERIFY;
    }
    if(mfaType.equals(MfaType.GOOGLE_AUTHENTICATOR)) {
      return MfaClientName.GOOGLE_AUTHENTICATOR;
    }
    throw new BusinessException("No MfaClientName associated with MfaType " + mfaType.toString());
  }


}
