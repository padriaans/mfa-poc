package com.altruist.mfa.poc.mfaclient;


import com.altruist.mfa.poc.model.mfa.MfaVerificationRequestDto;
import com.altruist.mfa.poc.model.mfa.MfaVerificationServiceDto;
import com.altruist.mfa.poc.model.mfa.MfaVerificationValidationDto;

public interface MfaClient {

  enum MfaClientName {
    TWILIO_VERIFY, GOOGLE_AUTHENTICATOR
  }

  enum MfaType {
    SMS, EMAIL, VOICE, GOOGLE_AUTHENTICATOR
  }

  MfaVerificationServiceDto createVerificationService(MfaVerificationServiceDto verificationService) throws Exception;

  //---------------------------------------------------------------------------------------------------------------

  MfaVerificationRequestDto sendVerificationToken(MfaVerificationRequestDto mfaVerificationRequestDto) throws Exception;

  MfaVerificationRequestDto fetchVerification(MfaVerificationRequestDto mfaVerificationRequestDto) throws Exception;

  MfaVerificationRequestDto approveVerification(MfaVerificationRequestDto mfaVerificationRequestDto) throws Exception;

  MfaVerificationRequestDto cancelVerification(MfaVerificationRequestDto mfaVerificationRequestDto) throws Exception;

  MfaVerificationValidationDto validateVerificationToken(MfaVerificationValidationDto mfaVerificationValidationDto)
      throws Exception;

}
