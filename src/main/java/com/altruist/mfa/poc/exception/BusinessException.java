package com.altruist.mfa.poc.exception;

public class BusinessException extends Exception {

  public BusinessException(String message) {
    super(message);
  }

  public BusinessException(Exception e) {
    super(e);
  }

  public BusinessException(String message, Throwable originalException) {
    super(message, originalException);
  }


}
