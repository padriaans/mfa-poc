package com.altruist.mfa.poc.controller;

import com.altruist.mfa.poc.mfaclient.MfaClient.MfaType;
import com.altruist.mfa.poc.model.UserAccountDto;
import com.altruist.mfa.poc.model.mfa.MfaVerificationRequestDto;
import com.altruist.mfa.poc.model.mfa.MfaVerificationValidationDto;
import com.altruist.mfa.poc.service.UserAccountService;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/user", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class UserAccountController {

  private UserAccountService userAccountService;

  @Autowired
  public UserAccountController(UserAccountService userAccountService) {
    this.userAccountService = userAccountService;
  }

  @PostMapping(value = "/create")
  public ResponseEntity<?> createUserAccount(@RequestBody UserAccountDto userAccountDto) {
    try {
      userAccountDto = userAccountService.create(userAccountDto);
      return new ResponseEntity<UserAccountDto>(userAccountDto, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<Exception>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping(value = "/update")
  public ResponseEntity<?> updateUserAccount(@RequestBody UserAccountDto userAccountDto) {
    try {
      userAccountDto = userAccountService.update(userAccountDto);
      return new ResponseEntity<UserAccountDto>(userAccountDto, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<Exception>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


  @GetMapping(value = "/username/{username}")
  public ResponseEntity<?> getUserAccountByUsername(@PathVariable String username) {
    try {
      UserAccountDto dto = userAccountService.getUserAccountByUsername(username);
      return new ResponseEntity<UserAccountDto>(dto, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<Exception>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping(value = "/sendmfa/{username}")
  public ResponseEntity<?> sendVerificationRequest(@PathVariable String username, @RequestParam(required = false) MfaType mfaTypeOverride,
      @RequestParam(required = false) String destination) {
    try {
      MfaVerificationRequestDto dto = userAccountService.sendVerificationRequest(username, mfaTypeOverride, destination);
      if(dto == null) {
        // Not applicable based on user settings
        return new ResponseEntity<String>("", HttpStatus.I_AM_A_TEAPOT);
      }
      return new ResponseEntity<MfaVerificationRequestDto>(dto, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<Exception>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping(value = "/validatetoken/{username}/{token}")
  public ResponseEntity<?> validateMfaToken(@PathVariable String username, @PathVariable String token, @RequestParam(required = false) UUID overrideId) {
    try {
      MfaVerificationValidationDto dto = userAccountService.validateMfaToken(username, token, overrideId);
      return new ResponseEntity<MfaVerificationValidationDto>(dto, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<Exception>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping(value = "/google/qrcode/{username}")
  public void getGoogleAuthenticatorQrCode(@PathVariable String username, HttpServletRequest request, HttpServletResponse response) {
    try {
      byte[] qrCode = userAccountService.generateGoogleAuthenticatorQrCode(username);
      response.setContentType("image/png");
      response.getOutputStream().write(qrCode);
      response.setStatus(HttpServletResponse.SC_OK);
    } catch (Exception e) {
      response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
      try {
        response.getOutputStream().print(ExceptionUtils.getStackTrace(e));
      } catch (Exception ee) {
      }
    }
  }


}
