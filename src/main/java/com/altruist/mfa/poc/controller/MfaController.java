package com.altruist.mfa.poc.controller;

import com.altruist.mfa.poc.model.mfa.MfaVerificationRequestDto;
import com.altruist.mfa.poc.model.mfa.MfaVerificationServiceDto;
import com.altruist.mfa.poc.model.mfa.MfaVerificationValidationDto;
import com.altruist.mfa.poc.service.MfaService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.server.PathParam;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/mfa", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class MfaController {

  private MfaService mfaService;

  @Autowired
  public MfaController(MfaService mfaService) {
    this.mfaService = mfaService;
  }

  @PostMapping(value = "/verificationservice/create")
  public ResponseEntity<?> createVerificationService(@RequestBody MfaVerificationServiceDto verificationServiceDto) {
    try {
      verificationServiceDto = mfaService.createVerificationService(verificationServiceDto);
      return new ResponseEntity<MfaVerificationServiceDto>(verificationServiceDto, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<Exception>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping(value = "/sendtoken")
  public ResponseEntity<?> sendVerificationToken(@RequestBody MfaVerificationRequestDto mfaVerificationRequestDto) {
    try {
      mfaVerificationRequestDto = mfaService.sendVerificationToken(mfaVerificationRequestDto);
      return new ResponseEntity<MfaVerificationRequestDto>(mfaVerificationRequestDto, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<Exception>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping(value = "/fetchverification")
  public ResponseEntity<?> fetchVerification(@RequestBody MfaVerificationRequestDto smsVerificationRequestDto) {
    try {
      smsVerificationRequestDto = mfaService.fetchVerification(smsVerificationRequestDto);
      return new ResponseEntity<MfaVerificationRequestDto>(smsVerificationRequestDto, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<Exception>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping(value = "/approveverification")
  public ResponseEntity<?> approveVerification(@RequestBody MfaVerificationRequestDto smsVerificationRequestDto) {
    try {
      smsVerificationRequestDto = mfaService.approveVerification(smsVerificationRequestDto);
      return new ResponseEntity<MfaVerificationRequestDto>(smsVerificationRequestDto, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<Exception>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping(value = "/cancelverification")
  public ResponseEntity<?> cancelVerification(@RequestBody MfaVerificationRequestDto smsVerificationRequestDto) {
    try {
      smsVerificationRequestDto = mfaService.cancelVerification(smsVerificationRequestDto);
      return new ResponseEntity<MfaVerificationRequestDto>(smsVerificationRequestDto, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<Exception>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping(value = "/validatetoken")
  public ResponseEntity<?> validateVerificationToken(@RequestBody MfaVerificationValidationDto smsVerificationValidationDto) {
    try {
      smsVerificationValidationDto = mfaService.validateVerificationToken(smsVerificationValidationDto);
      return new ResponseEntity<MfaVerificationValidationDto>(smsVerificationValidationDto, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<Exception>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping(value = "/google/qrcode/{account}")
  public void getGoogleAuthenticatorQrCode(@PathVariable String account, HttpServletRequest request, HttpServletResponse response) {
    try {
      byte[] qrCode = mfaService.generateGoogleAuthenticatorQrCode(account);
      response.setContentType("image/png");
      response.getOutputStream().write(qrCode);
      response.setStatus(HttpServletResponse.SC_OK);
    } catch (Exception e) {
      response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
      try {
        response.getOutputStream().print(ExceptionUtils.getStackTrace(e));
      } catch (Exception ee) {
      }
    }
  }

}