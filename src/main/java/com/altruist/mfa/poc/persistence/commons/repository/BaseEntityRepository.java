package com.altruist.mfa.poc.persistence.commons.repository;

import com.altruist.mfa.poc.persistence.commons.model.BaseEntity;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;


public interface BaseEntityRepository<T extends BaseEntity> extends JpaRepository<T, UUID> {

}
