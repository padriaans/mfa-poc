package com.altruist.mfa.poc.persistence.commons.exception;

import javax.persistence.PersistenceException;

public class EntityDoesNotExistException extends PersistenceException {

	public EntityDoesNotExistException(String message) {
		super(message);
	}
}
