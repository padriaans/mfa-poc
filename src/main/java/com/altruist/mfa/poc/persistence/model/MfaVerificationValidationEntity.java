package com.altruist.mfa.poc.persistence.model;

import com.altruist.mfa.poc.mfaclient.MfaClient.MfaClientName;
import com.altruist.mfa.poc.mfaclient.MfaClient.MfaType;
import com.altruist.mfa.poc.persistence.commons.model.BaseEntity;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "mfa_verification_validation")
@Cacheable(true)
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "mfa_verification_validation_id")) })
public class MfaVerificationValidationEntity extends BaseEntity {

  @Column(nullable = false)
  private String verificationValidationId;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_account_id", nullable = false)
  private UserAccountEntity userAccount;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "mfa_verification_request_id", nullable = true)
  private MfaVerificationRequestEntity mfaVerificationRequest;

  // Denormalized in case mfaVerificationRequest is null (Google Auth, for instance)
  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private MfaClientName mfaClientName;
  // Same
  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private MfaType mfaType;

  @Column(nullable = false)
  private String token;

  @Column(nullable = false)
  private Boolean validated;

}
