package com.altruist.mfa.poc.persistence.commons.exception;

import javax.persistence.PersistenceException;

public class EntityAlreadyExistsException extends PersistenceException {

	protected Object originalEntity;

	protected Object newEntity;

	public EntityAlreadyExistsException(String message, Throwable originalException, Object originalEntity,
			Object newEntity) {
		super(message, originalException);
		this.originalEntity = originalEntity;
		this.newEntity = newEntity;
	}

	public EntityAlreadyExistsException(String message, Object originalEntity, Object newEntity) {
		super(message);
		this.originalEntity = originalEntity;
		this.newEntity = newEntity;
	}

	public EntityAlreadyExistsException(String message, Throwable originalException, Object newEntity) {
		this(message, originalException, null, newEntity);
		this.newEntity = newEntity;
	}

	public Object getOriginalEntity() {
		return originalEntity;
	}

	public Object getNewEntity() {
		return newEntity;
	}

}
