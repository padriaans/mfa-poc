package com.altruist.mfa.poc.persistence.repository;

import com.altruist.mfa.poc.persistence.commons.repository.BaseEntityRepository;
import com.altruist.mfa.poc.persistence.model.UserAccountEntity;
import java.util.Optional;
import java.util.UUID;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAccountRepository extends BaseEntityRepository<UserAccountEntity> {

  Optional<UserAccountEntity> findByUsername(String username);

}
