package com.altruist.mfa.poc.persistence.commons.model;

import java.util.Date;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

public class BaseEntityListener {

  @PrePersist
  @PreUpdate
  public void setDates(BaseEntity baseEntity) {
    Date now = new Date();
    if (baseEntity.getDateTimeCreated() == null) {
      baseEntity.setDateTimeCreated(now);
    }
    baseEntity.setDateTimeUpdated(now);
  }

}
