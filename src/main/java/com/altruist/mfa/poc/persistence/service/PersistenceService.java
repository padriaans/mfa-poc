package com.altruist.mfa.poc.persistence.service;

import com.altruist.mfa.poc.persistence.model.MfaVerificationRequestEntity;
import com.altruist.mfa.poc.persistence.model.MfaVerificationValidationEntity;
import com.altruist.mfa.poc.persistence.model.UserAccountEntity;
import java.util.List;
import java.util.UUID;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PersistenceService {

  UserAccountEntity save(UserAccountEntity userAccountEntity);

  UserAccountEntity getUserAccountEntityByUsername(String username);

  UserAccountEntity getUserAccountEntityById(UUID id);

  //----------------------------------------------------------------------------------------------------

  MfaVerificationRequestEntity getLatestMfaVerificationRequestEntityForUserAccount(UserAccountEntity userAccountEntity);

  MfaVerificationRequestEntity save(MfaVerificationRequestEntity mfaVerificationRequestEntity);

  List<MfaVerificationRequestEntity> getMfaVerificationRequestEntityListForUserAccount(UserAccountEntity userAccountEntity, Pageable pageable);

  MfaVerificationRequestEntity getMfaVerificationRequestEntityByVerificationRequestId(String verificationRequestId);

  MfaVerificationRequestEntity getMfaVerificationRequestEntityById(UUID id);

  MfaVerificationRequestEntity getMfaVerificationRequestByOverrideId(UUID overrideId);

  //----------------------------------------------------------------------------------------------------

  MfaVerificationValidationEntity save(MfaVerificationValidationEntity mfaVerificationValidationEntity);

  MfaVerificationValidationEntity getMfaVerificationValidationEntityById(String verificationValidationId);

  Page<MfaVerificationValidationEntity> getMfaVerificationValidationEntityListForUserAccount(UserAccountEntity userAccount, Pageable pageable);

  Page<MfaVerificationValidationEntity> getMfaVerificationValidationEntityListForUserAccountAndMfaVerificationRequest(UserAccountEntity userAccount,
      MfaVerificationRequestEntity mfaVerificationRequest, Pageable pageable);



}
