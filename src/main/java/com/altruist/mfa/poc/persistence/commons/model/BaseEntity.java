package com.altruist.mfa.poc.persistence.commons.model;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import lombok.Data;

@Data
@MappedSuperclass
@EntityListeners({BaseEntityListener.class})
public class BaseEntity implements Serializable {

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;

  @Version
  @Column(name = "persistence_version")
  private Integer persistenceVersion;

  @Temporal(value = TemporalType.TIMESTAMP)
  @Column(name = "date_time_created", nullable = false)
  private Date dateTimeCreated;

  @Temporal(value = TemporalType.TIMESTAMP)
  @Column(name = "date_time_updated", nullable = false)
  private Date dateTimeUpdated;
}
