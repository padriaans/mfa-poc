package com.altruist.mfa.poc.persistence.model;

import com.altruist.mfa.poc.mfaclient.MfaClient.MfaType;
import com.altruist.mfa.poc.persistence.commons.model.BaseEntity;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.annotations.Type;


@Data
@Entity
@Table(name = "user_account")
@Cacheable(true)
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "user_account_id")) })
public class UserAccountEntity extends BaseEntity {

  @Column(nullable = false)
  private String firstName;

  @Column(nullable = false)
  private String lastName;

  @Column(nullable = false, unique = true)
  private String username;

  @Column(nullable = false)
  private String password;

  @Column(nullable = false, unique = true)
  private String emailAddress;

  private String phoneNumber;

  @Column(nullable = false)
  @Type(type ="yes_no")
  private Boolean useMfa = false;

  @Enumerated(EnumType.STRING)
  private MfaType mfaType;

  @OneToMany(mappedBy = "userAccount", fetch = FetchType.LAZY)
  private Set<MfaVerificationRequestEntity> mfaVerificationRequests;

}
