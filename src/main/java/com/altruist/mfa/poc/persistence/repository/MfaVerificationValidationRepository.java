package com.altruist.mfa.poc.persistence.repository;

import com.altruist.mfa.poc.persistence.commons.repository.BaseEntityRepository;
import com.altruist.mfa.poc.persistence.model.MfaVerificationRequestEntity;
import com.altruist.mfa.poc.persistence.model.MfaVerificationValidationEntity;
import com.altruist.mfa.poc.persistence.model.UserAccountEntity;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MfaVerificationValidationRepository extends BaseEntityRepository<MfaVerificationValidationEntity> {

  Optional<MfaVerificationValidationEntity> findByVerificationValidationId(String verificationValidationId);

  Page<MfaVerificationValidationEntity> findByUserAccount(UserAccountEntity userAccount, Pageable pageable);

  Page<MfaVerificationValidationEntity> findByUserAccountAndMfaVerificationRequest(UserAccountEntity userAccount,
      MfaVerificationRequestEntity mfaVerificationRequest, Pageable pageable);

}
