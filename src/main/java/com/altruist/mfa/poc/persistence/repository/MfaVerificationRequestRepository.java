package com.altruist.mfa.poc.persistence.repository;

import com.altruist.mfa.poc.persistence.commons.repository.BaseEntityRepository;
import com.altruist.mfa.poc.persistence.model.MfaVerificationRequestEntity;
import com.altruist.mfa.poc.persistence.model.UserAccountEntity;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MfaVerificationRequestRepository extends BaseEntityRepository<MfaVerificationRequestEntity> {

  Optional<MfaVerificationRequestEntity> findByVerificationRequestId(String verificationRequestId);

  Optional<MfaVerificationRequestEntity> findByOverrideId(UUID overrideId);

  Page<MfaVerificationRequestEntity> findByUserAccount(UserAccountEntity userAccount, Pageable pageable);


}
