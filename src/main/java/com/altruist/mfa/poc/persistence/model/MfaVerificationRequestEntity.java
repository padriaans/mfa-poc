package com.altruist.mfa.poc.persistence.model;

import com.altruist.mfa.poc.mfaclient.MfaClient.MfaClientName;
import com.altruist.mfa.poc.mfaclient.MfaClient.MfaType;
import com.altruist.mfa.poc.persistence.commons.model.BaseEntity;
import java.util.UUID;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.annotations.Type;

@Data
@Entity
@Table(name = "mfa_verification_request")
@Cacheable(true)
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "mfa_verification_request_id")) })
public class MfaVerificationRequestEntity extends BaseEntity {

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "user_account_id", nullable = false)
  private UserAccountEntity userAccount;

  @Column(unique = true)
  private UUID overrideId;

  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private MfaClientName mfaClientName;

  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private MfaType mfaType;

  @Column(nullable = false)
  private String destination;

  @Column(nullable = false)
  private String verificationServiceId;

  @Column(nullable = false, unique = true)
  private String verificationRequestId;

  @Column(nullable = false)
  @Type(type ="yes_no")
  private Boolean validated = false;


}
