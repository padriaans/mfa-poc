package com.altruist.mfa.poc.persistence.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.altruist.mfa.poc.persistence.model.MfaVerificationRequestEntity;
import com.altruist.mfa.poc.persistence.model.MfaVerificationValidationEntity;
import com.altruist.mfa.poc.persistence.model.UserAccountEntity;
import com.altruist.mfa.poc.persistence.repository.MfaVerificationRequestRepository;
import com.altruist.mfa.poc.persistence.repository.MfaVerificationValidationRepository;
import com.altruist.mfa.poc.persistence.repository.UserAccountRepository;
import com.altruist.mfa.poc.persistence.service.PersistenceService;

@Component
public class PersistenceServiceJpaImpl implements PersistenceService {

  private UserAccountRepository userAccountRepository;
  private MfaVerificationRequestRepository mfaVerificationRequestRepository;
  private MfaVerificationValidationRepository mfaVerificationValidationRepository;

  @Autowired
  public PersistenceServiceJpaImpl(UserAccountRepository userAccountRepository,
      MfaVerificationRequestRepository mfaVerificationRequestRepository,
      MfaVerificationValidationRepository mfaVerificationValidationRepository) {
    this.userAccountRepository = userAccountRepository;
    this.mfaVerificationRequestRepository = mfaVerificationRequestRepository;
    this.mfaVerificationValidationRepository = mfaVerificationValidationRepository;
  }

  @Override
  @Transactional
  public UserAccountEntity save(UserAccountEntity userAccountEntity) {
    return userAccountRepository.save(userAccountEntity);
  }

  // ----------------------------------------------------------------------------------------------------

  @Override
  public UserAccountEntity getUserAccountEntityByUsername(String username) {
    Optional<UserAccountEntity> optional = userAccountRepository.findByUsername(username);
    if (optional.isPresent()) {
      return optional.get();
    } else {
      return null;
    }
  }

  @Override
  public UserAccountEntity getUserAccountEntityById(UUID id) {
    Optional<UserAccountEntity> optional = userAccountRepository.findById(id);
    if (optional.isPresent()) {
      return optional.get();
    } else {
      return null;
    }
  }

  // ----------------------------------------------------------------------------------------------------

  @Override
  public MfaVerificationRequestEntity getLatestMfaVerificationRequestEntityForUserAccount(
      UserAccountEntity userAccountEntity) {
    Sort sort = Sort.by(new Order(Direction.DESC, "dateTimeCreated"));
    Pageable pageable = PageRequest.of(0, 1, sort);
    Page<MfaVerificationRequestEntity> page =
        mfaVerificationRequestRepository.findByUserAccount(userAccountEntity, pageable);
    if (!page.isEmpty()) {
      return page.getContent().get(0);
    }
    return null;
  }

  @Override
  @Transactional
  public MfaVerificationRequestEntity save(
      MfaVerificationRequestEntity mfaVerificationRequestEntity) {
    // Check if this is a dupe verificationRequestId, which can happen if a code is requested
    // while one that was issued is still valid and has not been verified yet. In that case, the
    // same code will be sent, attached to the same verificationRequestId. If it is a dupe, return
    // the older record
    MfaVerificationRequestEntity olderVersion =
        getMfaVerificationRequestEntityByVerificationRequestId(
            mfaVerificationRequestEntity.getVerificationRequestId());
    if (olderVersion != null) {
      return olderVersion;
    }
    return mfaVerificationRequestRepository.save(mfaVerificationRequestEntity);
  }

  @Override
  public List<MfaVerificationRequestEntity> getMfaVerificationRequestEntityListForUserAccount(
      UserAccountEntity userAccountEntity, Pageable pageable) {
    return mfaVerificationRequestRepository.findByUserAccount(userAccountEntity, pageable).toList();
  }

  @Override
  public MfaVerificationRequestEntity getMfaVerificationRequestEntityByVerificationRequestId(
      String verificationRequestId) {
    Optional<MfaVerificationRequestEntity> optional =
        mfaVerificationRequestRepository.findByVerificationRequestId(verificationRequestId);
    if (optional.isPresent()) {
      return optional.get();
    } else {
      return null;
    }
  }

  @Override
  public MfaVerificationRequestEntity getMfaVerificationRequestEntityById(UUID id) {
    Optional<MfaVerificationRequestEntity> optional = mfaVerificationRequestRepository.findById(id);
    if (optional.isPresent()) {
      return optional.get();
    } else {
      return null;
    }
  }

  @Override
  public MfaVerificationRequestEntity getMfaVerificationRequestByOverrideId(UUID overrideId) {
    Optional<MfaVerificationRequestEntity> optional = mfaVerificationRequestRepository.findByOverrideId(overrideId);
    if (optional.isPresent()) {
      return optional.get();
    } else {
      return null;
    }
  }

  @Override
  public MfaVerificationValidationEntity save(
      MfaVerificationValidationEntity mfaVerificationValidationEntity) {
    return mfaVerificationValidationRepository.save(mfaVerificationValidationEntity);
  }

  @Override
  public MfaVerificationValidationEntity getMfaVerificationValidationEntityById(
      String verificationValidationId) {
    Optional<MfaVerificationValidationEntity> optional = mfaVerificationValidationRepository
        .findByVerificationValidationId(verificationValidationId);
    if (optional.isPresent()) {
      return optional.get();
    } else {
      return null;
    }
  }

  @Override
  public Page<MfaVerificationValidationEntity> getMfaVerificationValidationEntityListForUserAccount(
      UserAccountEntity userAccount, Pageable pageable) {
    return mfaVerificationValidationRepository.findByUserAccount(userAccount, pageable);
  }

  @Override
  public Page<MfaVerificationValidationEntity> getMfaVerificationValidationEntityListForUserAccountAndMfaVerificationRequest(
      UserAccountEntity userAccount, MfaVerificationRequestEntity mfaVerificationRequest,
      Pageable pageable) {
    return mfaVerificationValidationRepository
        .findByUserAccountAndMfaVerificationRequest(userAccount, mfaVerificationRequest, pageable);
  }

  // ----------------------------------------------------------------------------------------------------

}
