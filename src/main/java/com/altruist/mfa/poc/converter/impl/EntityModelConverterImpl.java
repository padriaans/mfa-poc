package com.altruist.mfa.poc.converter.impl;

import com.altruist.mfa.poc.converter.EntityModelConverter;
import com.altruist.mfa.poc.model.BaseDto;
import com.altruist.mfa.poc.model.UserAccountDto;
import com.altruist.mfa.poc.model.mfa.MfaVerificationRequestDto;
import com.altruist.mfa.poc.model.mfa.MfaVerificationValidationDto;
import com.altruist.mfa.poc.persistence.commons.model.BaseEntity;
import com.altruist.mfa.poc.persistence.model.MfaVerificationRequestEntity;
import com.altruist.mfa.poc.persistence.model.MfaVerificationValidationEntity;
import com.altruist.mfa.poc.persistence.model.UserAccountEntity;
import com.altruist.mfa.poc.persistence.service.PersistenceService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Component
public class EntityModelConverterImpl implements EntityModelConverter {

  protected void mapBaseFields(BaseEntity source, BaseDto destination) {
    destination.setId(source.getId());
    destination.setDateTimeCreated(source.getDateTimeCreated());
    destination.setDateTimUpdated(source.getDateTimeUpdated());
  }

  protected void mapUserAccount(UserAccountDto UserAccountDto, UserAccountEntity UserAccountEntity) {
    UserAccountEntity.setFirstName(UserAccountDto.getFirstName());
    UserAccountEntity.setLastName(UserAccountDto.getLastName());
    UserAccountEntity.setUsername(UserAccountDto.getUsername());
    UserAccountEntity.setPassword(UserAccountDto.getPassword());
    UserAccountEntity.setEmailAddress(UserAccountDto.getEmailAddress());
    UserAccountEntity.setPhoneNumber(UserAccountDto.getPhoneNumber());
    UserAccountEntity.setUseMfa(UserAccountDto.getUseMfa());
    UserAccountEntity.setMfaType(UserAccountDto.getMfaType());
  }

  /**
   * User for create operations
   *
   * @param UserAccountDto
   * @return
   */
  @Override
  public UserAccountEntity makeUserAccountEntity(UserAccountDto UserAccountDto) {
    if(UserAccountDto == null) {
      return null;
    }
    UserAccountEntity UserAccountEntity = new UserAccountEntity();
    this.mapUserAccount(UserAccountDto, UserAccountEntity);
    return UserAccountEntity;
  }

  /**
   * User for update operations
   *
   * @param UserAccountDto
   * @param UserAccountEntity
   * @return
   */
  @Override
  public UserAccountEntity makeUserAccountEntity(UserAccountDto UserAccountDto, UserAccountEntity UserAccountEntity) {
    if(UserAccountDto == null) {
      return null;
    }
    this.mapUserAccount(UserAccountDto, UserAccountEntity);
    return UserAccountEntity;
  }

  @Override
  public UserAccountDto makeUserAccountDto(UserAccountEntity UserAccountEntity) {
    if(UserAccountEntity == null) {
      return null;
    }
    UserAccountDto UserAccountDto = new UserAccountDto();
    this.mapBaseFields(UserAccountEntity, UserAccountDto);
    UserAccountDto.setFirstName(UserAccountEntity.getFirstName());
    UserAccountDto.setLastName(UserAccountEntity.getLastName());
    UserAccountDto.setUsername(UserAccountEntity.getUsername());
    UserAccountDto.setPassword(UserAccountEntity.getPassword());
    UserAccountDto.setEmailAddress(UserAccountEntity.getEmailAddress());
    UserAccountDto.setPhoneNumber(UserAccountEntity.getPhoneNumber());
    UserAccountDto.setUseMfa(UserAccountEntity.getUseMfa());
    UserAccountDto.setMfaType(UserAccountEntity.getMfaType());
    return UserAccountDto;
  }

  @Override
  public List<UserAccountDto> makeUserAccountDtoList(Collection<UserAccountEntity> entities) {
    List<UserAccountDto> list = new ArrayList<>();
    if (!CollectionUtils.isEmpty(entities)) {
      for (UserAccountEntity entity : entities) {
        list.add(makeUserAccountDto(entity));
      }
    }
    return list;
  }

  //----------------------------------------------------------------------------------------------------

  @Override
  public MfaVerificationRequestDto makeMfaVerificationRequestDto(MfaVerificationRequestEntity entity) {
    if(entity == null) {
      return null;
    }
    MfaVerificationRequestDto dto = new MfaVerificationRequestDto();
    mapBaseFields(entity, dto);
    dto.setUserAccountId(entity.getUserAccount().getId());
    dto.setVerificationRequestId(entity.getVerificationRequestId());
    dto.setVerificationServiceId(entity.getVerificationServiceId());
    dto.setDestination(entity.getDestination());
    dto.setMfaType(entity.getMfaType());
    dto.setMfaClientName(entity.getMfaClientName());
    dto.setValidated(entity.getValidated());
    dto.setOverrideId(entity.getOverrideId());
    return dto;
  }

  @Override
  public List<MfaVerificationRequestDto> makeMfaVerificationRequestDtoList(Collection<MfaVerificationRequestEntity> entities) {
    List<MfaVerificationRequestDto> list = new ArrayList<>();
    if (!CollectionUtils.isEmpty(entities)) {
      for (MfaVerificationRequestEntity entity : entities) {
        list.add(makeMfaVerificationRequestDto(entity));
      }
    }
    return list;
  }

  //----------------------------------------------------------------------------------------------------

  @Override
  public MfaVerificationValidationDto makeMfaVerificationValidationDto(MfaVerificationValidationEntity entity) {
    if(entity == null) {
      return null;
    }
    MfaVerificationValidationDto dto = new MfaVerificationValidationDto();
    mapBaseFields(entity, dto);
    dto.setUserAccountId(entity.getUserAccount().getId());
    dto.setVerificationValidationId(entity.getVerificationValidationId());
    if(entity.getMfaVerificationRequest() != null) {
      dto.setMfaVerificationRequestId(entity.getMfaVerificationRequest().getId());
      dto.setVerificationServiceId(entity.getMfaVerificationRequest().getVerificationServiceId());
      dto.setVerificationRequestId(entity.getMfaVerificationRequest().getVerificationRequestId());
      dto.setDestination(entity.getMfaVerificationRequest().getDestination());
      dto.setOverrideId(entity.getMfaVerificationRequest().getOverrideId());
    }
    dto.setToken(entity.getToken());
    dto.setValidated(entity.getValidated());
    dto.setMfaType(entity.getMfaType());
    dto.setMfaClientName(entity.getMfaClientName());
    return dto;
  }

  @Override
  public List<MfaVerificationValidationDto> makeMfaVerificationValidationDtoList(Collection<MfaVerificationValidationEntity> entities) {
    List<MfaVerificationValidationDto> list = new ArrayList<>();
    if (!CollectionUtils.isEmpty(entities)) {
      for (MfaVerificationValidationEntity entity : entities) {
        list.add(makeMfaVerificationValidationDto(entity));
      }
    }
    return list;
  }
}