package com.altruist.mfa.poc.converter;

import com.altruist.mfa.poc.model.UserAccountDto;
import com.altruist.mfa.poc.model.mfa.MfaVerificationRequestDto;
import com.altruist.mfa.poc.model.mfa.MfaVerificationValidationDto;
import com.altruist.mfa.poc.persistence.model.MfaVerificationRequestEntity;
import com.altruist.mfa.poc.persistence.model.MfaVerificationValidationEntity;
import com.altruist.mfa.poc.persistence.model.UserAccountEntity;
import java.util.Collection;
import java.util.List;

public interface EntityModelConverter {

  UserAccountEntity makeUserAccountEntity(UserAccountDto UserAccountDto);

  UserAccountEntity makeUserAccountEntity(UserAccountDto UserAccountDto, UserAccountEntity UserAccountEntity);

  UserAccountDto makeUserAccountDto(UserAccountEntity UserAccountEntity);

  List<UserAccountDto> makeUserAccountDtoList(Collection<UserAccountEntity> entities);

  //----------------------------------------------------------------------------------------------------

  MfaVerificationRequestDto makeMfaVerificationRequestDto(MfaVerificationRequestEntity mfaVerificationRequestEntity);
  
  List<MfaVerificationRequestDto> makeMfaVerificationRequestDtoList(Collection<MfaVerificationRequestEntity> entities);

  //----------------------------------------------------------------------------------------------------

  MfaVerificationValidationDto makeMfaVerificationValidationDto(MfaVerificationValidationEntity mfaVerificationValidationEntity);

  List<MfaVerificationValidationDto> makeMfaVerificationValidationDtoList(Collection<MfaVerificationValidationEntity> entities);

}
