package com.altruist.mfa.poc.service.impl;

import com.altruist.mfa.poc.exception.BusinessException;
import com.altruist.mfa.poc.mfaclient.MfaClient;
import com.altruist.mfa.poc.mfaclient.MfaClient.MfaClientName;
import com.altruist.mfa.poc.mfaclient.MfaClient.MfaType;
import com.altruist.mfa.poc.mfaclient.MfaClientResolver;
import com.altruist.mfa.poc.mfaclient.impl.GoogleAuthenticatorClient;
import com.altruist.mfa.poc.model.mfa.MfaVerificationRequestDto;
import com.altruist.mfa.poc.model.mfa.MfaVerificationServiceDto;
import com.altruist.mfa.poc.model.mfa.MfaVerificationValidationDto;
import com.altruist.mfa.poc.service.MfaService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MfaServiceImpl implements MfaService {

  private MfaClientResolver mfaClientResolver;

  @Autowired
  public MfaServiceImpl(MfaClientResolver mfaClientResolver) {
    this.mfaClientResolver = mfaClientResolver;
  }

  @Override
  public MfaVerificationServiceDto createVerificationService(MfaVerificationServiceDto verificationService) throws BusinessException {
    try {
      MfaClient mfaClient = mfaClientResolver.getMfaClient(verificationService.getMfaClientName());
      verificationService = mfaClient.createVerificationService(verificationService);
      return verificationService;
    } catch (Exception e) {
      throw new BusinessException(e);
    }
  }

  protected MfaClient getMfaClient(MfaType mfaType, MfaClientName mfaClientName) throws BusinessException {
    if(mfaClientName != null) {
      return mfaClientResolver.getMfaClient(mfaClientName);
    } else {
      return mfaClientResolver.getMfaClient(mfaType);
    }
  }

  @Override
  public MfaVerificationRequestDto sendVerificationToken(MfaVerificationRequestDto mfaVerificationRequestDto) throws BusinessException {
    try {
      MfaClient mfaClient =  getMfaClient(mfaVerificationRequestDto.getMfaType(), mfaVerificationRequestDto.getMfaClientName());
      mfaVerificationRequestDto = mfaClient.sendVerificationToken(mfaVerificationRequestDto);
      return mfaVerificationRequestDto;
    } catch (Exception e) {
      throw new BusinessException(e);
    }
  }

  @Override
  public MfaVerificationRequestDto fetchVerification(MfaVerificationRequestDto mfaVerificationRequestDto) throws BusinessException {
    try {
      MfaClient mfaClient =  getMfaClient(mfaVerificationRequestDto.getMfaType(), mfaVerificationRequestDto.getMfaClientName());
      mfaVerificationRequestDto = mfaClient.fetchVerification(mfaVerificationRequestDto);
      return mfaVerificationRequestDto;
    } catch (Exception e) {
      throw new BusinessException(e);
    }
  }

  @Override
  public MfaVerificationRequestDto approveVerification(MfaVerificationRequestDto mfaVerificationRequestDto) throws BusinessException {
    try {
      MfaClient mfaClient =  getMfaClient(mfaVerificationRequestDto.getMfaType(), mfaVerificationRequestDto.getMfaClientName());
      mfaVerificationRequestDto = mfaClient.approveVerification(mfaVerificationRequestDto);
      return mfaVerificationRequestDto;
    } catch (Exception e) {
      throw new BusinessException(e);
    }
  }

  @Override
  public MfaVerificationRequestDto cancelVerification(MfaVerificationRequestDto mfaVerificationRequestDto) throws BusinessException {
    try {
      MfaClient mfaClient =  getMfaClient(mfaVerificationRequestDto.getMfaType(), mfaVerificationRequestDto.getMfaClientName());
      mfaVerificationRequestDto = mfaClient.cancelVerification(mfaVerificationRequestDto);
      return mfaVerificationRequestDto;
    } catch (Exception e) {
      throw new BusinessException(e);
    }
  }

  @Override
  public MfaVerificationValidationDto validateVerificationToken(MfaVerificationValidationDto mfaVerificationValidationDto)
      throws BusinessException {
    try {
      MfaClient mfaClient =  getMfaClient(mfaVerificationValidationDto.getMfaType(), mfaVerificationValidationDto.getMfaClientName());
      mfaVerificationValidationDto = mfaClient.validateVerificationToken(mfaVerificationValidationDto);
      return mfaVerificationValidationDto;
    } catch (Exception e) {
      throw new BusinessException(e);
    }
  }

  @Override
  public byte[] generateGoogleAuthenticatorQrCode(String account) throws BusinessException {
    try {
      GoogleAuthenticatorClient client = (GoogleAuthenticatorClient) mfaClientResolver.getMfaClient(MfaClientName.GOOGLE_AUTHENTICATOR);
      return client.createQRCode(account);
    } catch (Exception e) {
      throw new BusinessException(e);
    }
  }
}
