package com.altruist.mfa.poc.service;

import com.altruist.mfa.poc.exception.BusinessException;
import com.altruist.mfa.poc.mfaclient.MfaClient.MfaType;
import com.altruist.mfa.poc.model.UserAccountDto;
import com.altruist.mfa.poc.model.mfa.MfaVerificationRequestDto;
import com.altruist.mfa.poc.model.mfa.MfaVerificationValidationDto;
import java.util.UUID;

public interface UserAccountService {

  UserAccountDto create(UserAccountDto userAccountDto) throws BusinessException;

  UserAccountDto update(UserAccountDto userAccountDto) throws BusinessException;

  UserAccountDto getUserAccountByUsername(String username) throws BusinessException;

  MfaVerificationRequestDto sendVerificationRequest(String username, MfaType mfaTypeOverride, String destination) throws BusinessException;

  MfaVerificationValidationDto validateMfaToken(String username, String token, UUID overrideId) throws BusinessException;

  byte[] generateGoogleAuthenticatorQrCode(String username) throws BusinessException;
}
