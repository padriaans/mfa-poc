package com.altruist.mfa.poc.service.impl;

import com.altruist.mfa.poc.converter.EntityModelConverter;
import com.altruist.mfa.poc.exception.BusinessException;
import com.altruist.mfa.poc.mfaclient.MfaClient;
import com.altruist.mfa.poc.mfaclient.MfaClient.MfaClientName;
import com.altruist.mfa.poc.mfaclient.MfaClient.MfaType;
import com.altruist.mfa.poc.mfaclient.MfaClientResolver;
import com.altruist.mfa.poc.mfaclient.impl.GoogleAuthenticatorClient;
import com.altruist.mfa.poc.model.UserAccountDto;
import com.altruist.mfa.poc.model.mfa.MfaVerificationRequestDto;
import com.altruist.mfa.poc.model.mfa.MfaVerificationValidationDto;
import com.altruist.mfa.poc.persistence.model.MfaVerificationRequestEntity;
import com.altruist.mfa.poc.persistence.model.MfaVerificationValidationEntity;
import com.altruist.mfa.poc.persistence.model.UserAccountEntity;
import com.altruist.mfa.poc.persistence.service.PersistenceService;
import com.altruist.mfa.poc.service.UserAccountService;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class UserAccountServiceImpl implements UserAccountService {

  private EntityModelConverter entityModelConverter;
  private PersistenceService persistenceService;
  private MfaClientResolver mfaClientResolver;

  @Autowired
  public UserAccountServiceImpl(EntityModelConverter entityModelConverter,
      PersistenceService persistenceService, MfaClientResolver mfaClientResolver) {
    this.entityModelConverter = entityModelConverter;
    this.persistenceService = persistenceService;
    this.mfaClientResolver = mfaClientResolver;
  }

  @Override
  public UserAccountDto create(UserAccountDto userAccountDto) throws BusinessException {
    UserAccountEntity userAccountEntity =
        entityModelConverter.makeUserAccountEntity(userAccountDto);
    userAccountEntity = persistenceService.save(userAccountEntity);
    return entityModelConverter.makeUserAccountDto(userAccountEntity);
  }

  @Override
  public UserAccountDto update(UserAccountDto userAccountDto) throws BusinessException {
    UserAccountEntity userAccountEntity =
        persistenceService.getUserAccountEntityById(userAccountDto.getId());
    if (userAccountEntity == null) {
      throw new BusinessException("Unknown user account " + userAccountDto.getId());
    }
    userAccountEntity =
        entityModelConverter.makeUserAccountEntity(userAccountDto, userAccountEntity);
    userAccountEntity = persistenceService.save(userAccountEntity);
    return entityModelConverter.makeUserAccountDto(userAccountEntity);
  }

  @Override
  public UserAccountDto getUserAccountByUsername(String username) throws BusinessException {
    UserAccountEntity userAccountEntity =
        persistenceService.getUserAccountEntityByUsername(username);
    return entityModelConverter.makeUserAccountDto(userAccountEntity);
  }

  protected UserAccountEntity getUserAccountForMfa(String username) throws BusinessException {
    UserAccountEntity userAccount = persistenceService.getUserAccountEntityByUsername(username);
    if (userAccount == null) {
      throw new BusinessException("Unknown user account \"" + username + "\"");
    }
    if (!userAccount.getUseMfa()) {
      throw new BusinessException(
          "User Account " + userAccount.getUsername() + " does not use MFA");
    }
    MfaType mfaType = userAccount.getMfaType();
    if (mfaType == null) {
      throw new BusinessException(
          "User Account " + userAccount.getUsername() + " has incorrect MFA settings");
    }
    return userAccount;
  }

  @Override
  @Transactional
  public MfaVerificationRequestDto sendVerificationRequest(String username, MfaType mfaTypeOverride, String destination)
      throws BusinessException {
    UserAccountEntity userAccount = getUserAccountForMfa(username);
    MfaType mfaType = null;
    if(mfaTypeOverride != null) {
      if (mfaTypeOverride.equals(MfaType.GOOGLE_AUTHENTICATOR)) {
        throw new BusinessException(MfaType.GOOGLE_AUTHENTICATOR + " is not a valid mfaTypeOverride");
      }
      if(StringUtils.isEmpty(destination)) {
        throw new BusinessException("destination must be specified when asking for an MFA type override");
      }
      mfaType = mfaTypeOverride;
    } else {

      mfaType = userAccount.getMfaType();
      if (mfaType.equals(MfaType.GOOGLE_AUTHENTICATOR)) {
        // Does not apply
        return null;
      }

      if (mfaType.equals(MfaType.SMS) || mfaType.equals(MfaType.VOICE)) {
        if (StringUtils.isEmpty(userAccount.getPhoneNumber())) {
          throw new BusinessException(
              "User Account " + userAccount.getUsername() + " does not have a phone number set");
        } else {
          destination = userAccount.getPhoneNumber();
        }
      }
      if (mfaType.equals(MfaType.EMAIL)) {
        if (StringUtils.isEmpty(userAccount.getEmailAddress())) {
          throw new BusinessException(
              "User Account " + userAccount.getUsername() + " does not have a email address set");
        } else {
          destination = userAccount.getEmailAddress();
        }
      }
    }

    MfaClientName mfaClientName = mfaClientResolver.getMfaClientName(mfaType);
    MfaClient mfaClient = mfaClientResolver.getMfaClient(mfaClientName);
    MfaVerificationRequestDto dto = new MfaVerificationRequestDto();
    dto.setMfaType(mfaType);
    dto.setDestination(destination);
    try {
      dto = mfaClient.sendVerificationToken(dto);
    } catch (Exception e) {
      throw new BusinessException(e);
    }
    MfaVerificationRequestEntity entity = new MfaVerificationRequestEntity();
    if(mfaTypeOverride != null) {
      entity.setOverrideId(UUID.randomUUID());
    }
    entity.setUserAccount(userAccount);
    entity.setMfaType(mfaType);
    entity.setMfaClientName(mfaClientName);
    entity.setDestination(destination);
    entity.setVerificationRequestId(dto.getVerificationRequestId());
    entity.setVerificationServiceId(dto.getVerificationServiceId());
    entity.setValidated(false); // Always false upon creation
    entity = this.persistenceService.save(entity);

    return this.entityModelConverter.makeMfaVerificationRequestDto(entity);
  }

  @Override
  @Transactional
  public MfaVerificationValidationDto validateMfaToken(String username, String token, UUID overrideId)
      throws BusinessException {
    MfaType mfaType = null;
    MfaClientName mfaClientName = null;
    UserAccountEntity userAccountEntity = getUserAccountForMfa(username);
    MfaVerificationRequestEntity requestEntity = null;
    if(overrideId != null) {
      requestEntity =
          persistenceService.getMfaVerificationRequestByOverrideId(overrideId);
      if (requestEntity == null) {
        throw new BusinessException(
            "Invalid overrideId " + overrideId);
      }
      if(!requestEntity.getUserAccount().getId().equals(userAccountEntity.getId())) {
        throw new BusinessException("This override does not belong to user " + userAccountEntity.getUsername());
      }
      if (requestEntity.getValidated()) {
        throw new BusinessException(
            "This override has already been validated. Please request a new token");
      }
      mfaType = requestEntity.getMfaType();
      mfaClientName = requestEntity.getMfaClientName();
    } else {
      if (userAccountEntity.getMfaType().equals(MfaType.EMAIL)
          || userAccountEntity.getMfaType().equals(MfaType.SMS)
          || userAccountEntity.getMfaType().equals(MfaType.VOICE)) {
        // These methods imply the existence of a verification request. Load the most recent one
        requestEntity =
            persistenceService.getLatestMfaVerificationRequestEntityForUserAccount(userAccountEntity);
        if (requestEntity == null) {
          throw new BusinessException(
              "No Verification Request found for user account " + userAccountEntity.getUsername());
        }
        if (requestEntity.getValidated()) {
          throw new BusinessException(
              "Latest Verification Request for user account " + userAccountEntity.getUsername()
                  + " has already been validated. Please request a new token");
        }
        mfaType = requestEntity.getMfaType();
        mfaClientName = requestEntity.getMfaClientName();
      } else {
        // Other methods do not request a verification request, get the client name from user settings
        mfaType = userAccountEntity.getMfaType();
        mfaClientName = mfaClientResolver.getMfaClientName(mfaType);
      }
    }

    MfaClient mfaClient = mfaClientResolver.getMfaClient(mfaClientName);
    MfaVerificationValidationDto dto = new MfaVerificationValidationDto();
    if (requestEntity != null) {
      dto.setVerificationServiceId(requestEntity.getVerificationServiceId());
      dto.setDestination(requestEntity.getDestination());
    }
    dto.setToken(token);
    dto.setMfaClientName(mfaClientName);
    try {
      dto = mfaClient.validateVerificationToken(dto);
    } catch (Exception e) {
      throw new BusinessException(e);
    }

    if (requestEntity != null && dto.getValidated()) {
      requestEntity.setValidated(true);
      requestEntity = persistenceService.save(requestEntity);
    }

    MfaVerificationValidationEntity entity = new MfaVerificationValidationEntity();
    entity.setVerificationValidationId(dto.getVerificationValidationId());
    entity.setUserAccount(userAccountEntity);
    entity.setMfaType(mfaType);
    entity.setMfaClientName(mfaClientName);
    if (requestEntity != null) {
      entity.setMfaVerificationRequest(requestEntity);
    }
    entity.setToken(token);
    entity.setValidated(dto.getValidated());
    entity = persistenceService.save(entity);

    return entityModelConverter.makeMfaVerificationValidationDto(entity);
  }

  @Override
  public byte[] generateGoogleAuthenticatorQrCode(String username) throws BusinessException {
    UserAccountEntity userAccount = getUserAccountForMfa(username);
    MfaType mfaType = userAccount.getMfaType();
    if (!mfaType.equals(MfaType.GOOGLE_AUTHENTICATOR)) {
      throw new BusinessException(
          "User account " + username + " does not have MFA set to GoogleAuthenticator");
    }
    if (StringUtils.isEmpty(userAccount.getEmailAddress())) {
      throw new BusinessException(
          "User account " + username + " does not have an email address set");
    }
    try {
      GoogleAuthenticatorClient client = (GoogleAuthenticatorClient) mfaClientResolver
          .getMfaClient(MfaClientName.GOOGLE_AUTHENTICATOR);
      return client.createQRCode(userAccount.getEmailAddress());
    } catch (Exception e) {
      throw new BusinessException(e);
    }
  }
}
