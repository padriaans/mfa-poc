package com.altruist.mfa.poc.service;

import com.altruist.mfa.poc.exception.BusinessException;
import com.altruist.mfa.poc.model.mfa.MfaVerificationRequestDto;
import com.altruist.mfa.poc.model.mfa.MfaVerificationServiceDto;
import com.altruist.mfa.poc.model.mfa.MfaVerificationValidationDto;


public interface MfaService {

  MfaVerificationServiceDto createVerificationService(MfaVerificationServiceDto verificationService) throws BusinessException;

  MfaVerificationRequestDto sendVerificationToken(MfaVerificationRequestDto MfaVerificationRequestDto) throws BusinessException;

  MfaVerificationRequestDto fetchVerification(MfaVerificationRequestDto MfaVerificationRequestDto) throws BusinessException;

  MfaVerificationRequestDto approveVerification(MfaVerificationRequestDto MfaVerificationRequestDto) throws BusinessException;

  MfaVerificationRequestDto cancelVerification(MfaVerificationRequestDto MfaVerificationRequestDto) throws BusinessException;

  MfaVerificationValidationDto validateVerificationToken(MfaVerificationValidationDto MfaVerificationValidationDto)
      throws BusinessException;

  byte[] generateGoogleAuthenticatorQrCode(String account) throws BusinessException;
}
